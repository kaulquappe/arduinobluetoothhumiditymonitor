# ArduinoBluetoothHumidityMonitor  

[TOC]  

Monitor humidity and temperature with your mobile phone.  
Using an arduino nano board and a BLE CC41-A Bluetooth.  
See also:  [Arduino Nano Serial Bluetooth Chat](https://bitbucket.org/kaulquappe/arduinonanobluetoothchat/src/master/).  

![Monitor on mobile phone](SerialBluetooth02.png)


## Hardware
---
#### BLE CC41-A Bluetooth Module

I bought a Bluetooth 4.0 BLE module on [*www.aliexpress.com*](https://www.aliexpress.com/item/Android-IOS-BLE-Bluetooth-4-0-HM-10-CC2540-CC2541-6Pin-Serial-Wireless-Module-DC-5V/32837215626.html) and realized that it was not an original HM-10 module but a clone called BLE CC41-A bluetooth module based on the TI CC2541 chip.  
But that did not really matter to me at that time because I had no clue what the difference was anyway.  
These clones have a sort of slightly different/limited AT command set.  
You can find the documentation of these devices here:  
* [Datasheet of the original HM-10 module](http://duinopeak.com/wiki/images/c/cd/Bluetooth4_en.pdf)

* [CC41-A bluetooth module specification](https://img.banggood.com/file/products/20150104013145BLE-CC41-A%20Spefication.pdf)

* [AT-Command subset of the CC41-A bluetooth module](http://img.banggood.com/file/products/20150104013200BLE-CC41-A_AT%20Command.pdf).

* [Datasheet of the TI CC2541 ble bluetooth chip](http://www.ti.com/lit/ds/symlink/cc2541.pdf)

* [Turorial about the HM-10 Bluetooth Breakout Board ](https://how2electronics.com/bluetooth-low-energy-tutorial-with-hm-10-ble-4-0-arduino/#HM-10_Breakout_Board)

* [Troubleshooting page](http://www.kai-morich.de/android/ble_troubleshooting.html)

* [Difference between orignal HM-10 and CC41-A bluetooth module](https://blog.yavilevich.com/2016/12/hm-10-or-cc41-a-module-automatic-arduino-ble-module-identification)

I have also added these documents to this repository.

![CC2541 Frontside](CC2541_front.jpg) ![CC2541 Backside](CC2541_back.jpg)

### Wiring
---

|      Nano   |  Bluetooth |
|-------------|------------|
| PIN 2 (RX)  |    TXD     |
| PIN 1 (TX)  |    RXD     |


|     Nano    |    HTU21D  |
|-------------|------------|
|      A5     |    CL      |
|      A4     |    SA      |

![Wiring](BlueHumidity03.jpg)

```
This time I used hardware serial communication. 
In order to flash the arduino with the code you have to make sure that you disconnect the bluetooth board because otherwise it occupies the serial port used by the arduino ide !!
```
Power the HTU21D chip with 3.3 volt.  
Power the bluetooth device with 5 volt.  
Use level shifter for HTU21D CL + SA.  
Use level shifter for bluetooth CC41-A module RX + TX.   
The *STATE* pin of the bluetooth module is connected to a LED, which indicates clearly when a mobile phone is connected.  
The *EN* pin could be used to disconnect devices by pulling it to low.  

#### Logic Level Shifter
A level shifter translates the voltage levels from 5 volt to 3.3 volt and vice versa.  
It is used in order to deal with the different voltage levels of arduino nano (5volt) and HTU21D module (3.3 volt) and the CC41-A bluetooth module (3.3 volt).  
I bought mine from [aliexpress](https://www.aliexpress.com/item/5PCS-IIC-I2C-Logic-Level-Converter-Bi-Directional-Module-5V-to-3-3V-For-Arduino/32216849765.html).  

#### 5 volt to 3.3 volt regulator
In order to transform the 5 volt provided by an external power supply to 3.3 volt used by the HTU21D and the bluetooth module I bought [this setp-down-converter](https://www.aliexpress.com/item/DD0503MA-5-Ultra-mini-DC-3-7V-4-5V-5V-to-3-3V-DC-DC-Step/32787844898.html).

#### HTU21D Humidity and Temperature Sensor
This breakout board uses I2C communication and is connected directly to the I2C hardware pins of the arduino:  
 * Clock (CL) to arduino nano pin A5
 * Serial Data (SA) to arduino nano pin A4
I bought mine from [aliexpress](https://www.aliexpress.com/item/Temperature-Humidity-Sensor-GY-213V-HTU21D-I2C-Replace-SHT21-SI7021-HDC1080-Module/32476325114.html).

#### Arduino Nano
I bought a cheap chinese clone of the arduino nano on [*aliexpress*](https://www.aliexpress.com/item/1PCS-MINI-USB-Nano-V3-0-ATmega328P-CH340G-5V-16M-Micro-controller-board-for-arduino-NANO/32848298184.html).

![Nano Setup](BlueHumidity04.jpg)

### Arduino
---
You can download the newest arduino id from [*here*](https://www.arduino.cc/en/Main/Software) and follow the installations instructions [*here*](https://www.arduino.cc/en/Guide/HomePage).

### Serial Bluetooth Terminal
---
I used [*this serial bluetooth terminal*](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal) from [*Kai Morich*](http://www.kai-morich.de/android/).  
It was the only bluetooth terminal I found that was able to handle the BLE 4.0 paring without PIN.  
It seems as if there is still mismatch between what the standard says and how it is implemented by different chip producers and how it is handled by software like android for example.  
You can read more about that and how to troubleshoot it [*here*](http://www.kai-morich.de/android/ble_troubleshooting.html).  
***It is important that you use a bluetooth software that is able to talk with the BLE modules, otherwise you might end up debugging on the wrong place! :)***  

![BLE bluetooth devices](SerialBluetooth01.png)

## Misc
---
* Make sure you connect the RX of the HTU21D to the TX of the nano ( not RX to RX !)
* Make sure you connect the TX of the HTU21D to the RX of the nano ( not TX to TX !)
* Use a logical level shifter for HTU22D's CL and SD
* Use a logical level shifter for Bluetooths's RX and TX
* Disconnect the bluetooth module from power or/and TX/RX when u flash the arduino nano. Otherwise the harware serial is occupied and produces an error message.
* Use an app that is able to communicate with the newer bluetooth BLE 4.0 devices !
* No pairing is required with this bluetooth setup !

![Nano Setup](BlueHumidity01.jpg)

## Code
---
```C++
//////////////////////////////////////////////////
// Used with the HM-10/ CC2540 /CC2541
// BLE bluetooth devices.
// Make sure that you use an app that actually
// is able to make the pairing/bonding with
// the BLE bluetooth devices, otherwise
// it can lead to frustration ;).
// This one worked for me:
// https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal
//
// Make sure you connect: 
//    bluetooth RX to arduino nano TX (Pin 1)
//    bluetooth TX to arduino nano RX (Pin 2)
//    HTU21D CL to arduino nano A5
//    HTU21D DA to arduino nano A4 
// 
//    power HTU21D with 3.3 volt
//    power bluetooth HM-10 with 5 volt
//    use level shifter for HTU21D CL + SA
//    use level shifter for bluetooth HM-10 RX + TX 
//
// see also: https://bitbucket.org/kaulquappe/arduinonanobluetoothchat
//
// ATTENTION: 
//           since we use hardware serial pins here we have to 
//           disconnect the bluetooth device while flashing the code !
//
///////////////////////////////////////////////////

#include <Wire.h>
#include "SparkFunHTU21D.h"

HTU21D myHumidity;

String msg;
float  humd;
float  temp;

///////////////////////////////////////////////////
//
///////////////////////////////////////////////////
void setup()
{
  myHumidity.begin();

  Serial.begin(9600);
  Serial.println( "Serial Bluetooth Humidity and Temperature Monitor" );
  Serial.println( "           ver. 0.1 by kaulquappe"                 );

  //  Serial.println("AT+HELP");
  //  Serial.println("AT+TYPE0");  // no pin/pairing/bonding needed
  //  Serial.println("AT+RESET");  // would AT commands work here ?
}
///////////////////////////////////////////////////
//
///////////////////////////////////////////////////
void loop()
{
  humd = myHumidity.readHumidity();
  temp = myHumidity.readTemperature();

  msg = "Humidity: " + String(humd) + "%    Temperature: " + String(temp) + "° C\n";
  Serial.print( msg );
  delay(4000);
}
```

## License
---
Distributed under the Apache License Version 2.0.

## References
---
* https://blog.yavilevich.com/2017/03/mlt-bt05-ble-module-a-clone-of-a-clone/
* https://blog.yavilevich.com/2016/12/hm-10-or-cc41-a-module-automatic-arduino-ble-module-identification/
* https://www.instructables.com/id/Program-Your-Arduino-With-an-Android-Device-Over-B/
* https://create.arduino.cc/projecthub/achindra/bluetooth-le-using-cc-41a-hm-10-clone-d8708e
* http://www.martyncurrey.com/hm-10-bluetooth-4ble-modules/
* https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal
* http://jnhuamao.cn/bluetooth.asp?ID=1

## Author
---
* kaulquappe (https://bitbucket.org/kaulquappe)

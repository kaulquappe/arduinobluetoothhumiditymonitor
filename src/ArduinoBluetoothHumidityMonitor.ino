//////////////////////////////////////////////////
// Used with the HM-10/ CC2540 /CC2541
// BLE bluetooth devices.
// Make sure that you use an app that actually
// is able to make the pairing/bonding with
// the BLE bluetooth devices, otherwise
// it can lead to frustration ;).
// This one worked for me:
// https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal
//
// Make sure you connect: 
//    bluetooth RX to arduino nano TX (Pin 1)
//    bluetooth TX to arduino nano RX (Pin 2)
//    HTU21D CL to arduino nano A5
//    HTU21D DA to arduino nano A4 
// 
//    power HTU21D with 3.3 volt
//    power bluetooth HM-10 with 5 volt
//    use level shifter for HTU21D CL + SA
//    use level shifter for bluetooth HM-10 RX + TX 
//
// see also: https://bitbucket.org/kaulquappe/arduinonanobluetoothchat
//
// ATTENTION: 
//           since we use hardware serial pins here we have to 
//           disconnect the bluetooth device while flashing the code !
//
///////////////////////////////////////////////////

#include <Wire.h>
#include "SparkFunHTU21D.h"

HTU21D myHumidity;

String msg;
float  humd;
float  temp;

///////////////////////////////////////////////////
//
///////////////////////////////////////////////////
void setup()
{
  myHumidity.begin();

  Serial.begin(9600);
  Serial.println( "Serial Bluetooth Humidity and Temperature Monitor" );
  Serial.println( "           ver. 0.1 by kaulquappe"                 );

  //  Serial.println("AT+HELP");
  //  Serial.println("AT+TYPE0");  // no pin/pairing/bonding needed
  //  Serial.println("AT+RESET");  // would AT commands work here ?
}
///////////////////////////////////////////////////
//
///////////////////////////////////////////////////
void loop()
{
  humd = myHumidity.readHumidity();
  temp = myHumidity.readTemperature();

  msg = "Humidity: " + String(humd) + "%    Temperature: " + String(temp) + "° C\n";
  Serial.print( msg );
  delay(4000);
}

